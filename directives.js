var app = angular.module("app", []);

app.controller("SomeController", function($scope){
    $scope.expanders = [
        {title: 'Titre 1',
         text: 'Contenu 1'},
        {title: 'Titre 2',
         text: 'Contenu 2'},
        {title: 'Titre 3',
         text: 'Contenu 3'}
    ];
});

app.directive("accordion", function(){
    return{
        restrict: 'EA',
        replace: true,
        transclude: true,
        template: "<div ng-transclude></div>",
        controller: function(){
            var expanders = [];
            this.gotOpened = function(selectedExpander){
                expanders.forEach(function(expander){
                    if(selectedExpander != expander){
                        expander.showMe = false;
                    }
                });
            };
            this.addExpander = function(expander){
                expanders.push(expander);
            };
        }
    };
});

app.directive("expander", function(){
    return {
        restrict: 'EA',
        replace: true,
        transclude: true,
        require: '^?accordion',
        scope: {title: '=expanderTitle'},
        template: '<div>' +
        '<div class="title" ng-click="toggle()">{{title}}</div>' +
        '<div class="body" ng-show="showMe" ng-transclude></div>' +
        '</div>',
        link: function(scope, element, attrs, accordionController){
            scope.showMe = false;
            accordionController.addExpander(scope);
            scope.toggle = function toggle(){
                scope.showMe = !scope.showMe;
                accordionController.gotOpened(scope);
            };
        }
    };
});

/*restrict : permet de définir comment la déclaration de la directive doit se faire dans le code HTML
             option:-'E'pour un élément
                    -'A'pour un attribut
                    -'C'pour une classe
                    -'M'pour commentaires
  priority : dans le cas de plusieurs dirctives , il est possible d'accorder une priorité a une directive.les priorités
  hautes s'éxécute en premier.La valeur par défaut est 0.

  templates : permet le contenu d'un élément par un template(qui autorise l'écriture d'un code sans considération envers 
  le type des données avec lesquelles il sera finalement utilisé. Les templates introduisent le concept de programmation générique
   dans le langage.)

  templateUrl : option qui permet de charger un template depuis un fichier  
  
  replace : boolean, si true, remplace l'élément , si false ou non spécifié , ajoute la directive à l'élément courant
  
  transclude : au lieu de remplacer ou d'ajouter du contenu, on peut déplacer le contenu original à l'intérieur du nouveau template 
  grâce à cette propriété.Si true , la directive supprimera le contenu original et le rendra disponible pour le réinsérer à l'intérieur
  du template en utilisant la directive ng-translude  
  
  scope : créer un nouveau contexte au lieu d'hériter du contexte du parent.3 options:
         -utiliser le contexte existant qui est celui dans lequel évolue notre directive(valeur par défaut).
         -définir un nouveau contexte qui va hériter du contexte du parent(en l'occurence le contrôleur qui encapsule notre directive).
         -créer un contexte isolé qui n'hérite pas des propriétés du contexte du parent à moins que l 'on spécifie les attributs dont
         nous voulons hériter.
         voilà la syntaxe:
         -contexte existant : scope : false
         -nouveau contexte : scope : true
         -contexte isolé : scope {attributeName1: 'binding strategie',...}
         La stratégie de binding est définie selon 3 symboles:
         - @ : passer l'attribut sous forme de chaine de caractère .
         - = : utiliser le data-binding afin de lier la propriété à la propriété du parent.
         - & : passer une fonction depuis le parent qui sera exécuter plus tard.

  controller : créer un contrôleur,utile lorsque on a plusieurs directives que nous voulons faire communiquer.
  
  require : indique qu'une autre directive est requise pour que celle-ci fonctionne correctement.
         
  les fonctions compile et link vont être utilisées dans le processus de manipulation du Dom.
  Ce processus se fait en 3 étapes: 
  1.Chargement: Angular charge et recherche la directive ng-app.
  2.Compilation : Angular parcourt le DOM afin d'identifier l'ensemble des directives.Il s'occupe notamment des manipulations du DOM
  qui ne requièrent pas l'utilisation du contexte.
  3.Mise en place des liens : il s'agit de la dernière phase.C'est notamment au cours de celle-ci qu'angular se charge de lier un 
  contexte à la directive.
  
  Ici la fonction link permet de créer le modèle qui comprend la fonction permettant d'ouvrir et de fermer notre onglet
  
  ON A ACCES AU CONTEXTE SEULEMENT DANS LES FONCTIONS LINK ET NON PAS DANS LES FONCTONS COMPILE.
  
  LES FONCTIONS COMPILE NE SONT PAS APPELEES QU'UNE SEULE FOIS ALORS QUE LES FONCTIONS LINK SONT APPELEES PLUSIEURS FOIS,UNE 
  FOIS POUR CHAQUE INSTANCE DE LA DIRECTIVE.
  */