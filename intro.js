var myApp = angular.module('myApp',[]);
myApp.config(function(){});
//création du controller(regroupement de fonctions js)
angular.module('myApp').controller('monController',['$scope',function($scope){
    //le scope est une passerelle entre la vue et le controller
    $scope.text = "Hello";
    $scope.array = [1,2,3,4,5];
}]);    